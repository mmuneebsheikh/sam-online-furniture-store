
<?php
// Start the session
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Login/signup</title>
		<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" type="text/css" href="proto2Style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <script type="text/javascript" language="javascript" src="proto2JS.js"></script>
</head>
<body id="bg-body">

		<!-- NAVIGATION PANEL -->
<div class="container">

	<nav class="navbar-fixed-top" >
		<div class="container-fluid" style="padding-top: 30px;" >
			<div class="row" >
				<!-- LOGO -->
				<div class="col-sm-2" style="padding-left: 50px">
					<a href="home.php"><img src="imgs/logo/sam.png" width="100px" ></a>
				</div>
				<div class="col-sm-3" > <!-- SEARCH BAR -->
					<div class="input-group" style="padding-left: 50px; ">
						<input type="text" name="searchBar" placeholder="Search.." class="form-control">
						<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					</div>
				</div>

				<div class="col-sm-4"></div>
				<div class="col-sm-3" style="font-size: 12pt; font-family: serif;" >
					<!-- loginSignup -->
					<div style="margin-left: -90px;display: inline-block;">
						<span class="glyphicon glyphicon-earphone text-muted" ></span>
						<label class="navlbl" style="" >0399-9999999</label>
					</div>
						
						<div id="linklogin" style="display: inline-block;">
							<a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="login.php"> Login / Signup</label></a>
						</div>
							
						<div id="clientacc" style="display: inline-block;">
							<a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="signout.php"> Signout </label></a>
						</div>
							
						<div id="adminacc" style="display: inline-block;">
							<a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="adminpanel.php"> ACCOUNT </label></a>
						</div>
							
						<div style="display: inline-block;">
							<a href="cart.php" ><span class="glyphicon glyphicon-shopping-cart text-orange" ></span></a>
							<label class="navlbl"><a class="text-orange" href="cart.php"> My Cart </label></a>
						</div>
							
						
						<!--info-->

						<div class="col-sm-*" >
							<label class="text-orange" style="margin-bottom: 0px;">CASH ON DELIVERY | EASY PAY</label></br>
							<label style= "font-size: 12pt">Product Inquries/Checkout issues? Call Us</label>
						</div>
				</div>
					
			</div>
				
		<!-- PRIMARY MENU -->
			<div class="col-sm-12 prinav text-center" style="padding-bottom:10px  ;margin-right: 0px;margin-top: 20px; background-color: transparent;">
				<input class="btn btn-link navbtn" type="button" name="#BedRoom" value="Bed Room" onclick="window.location.href = 'bedroom.php'">
				<input class="btn btn-link navbtn" type="button" name="#Dining" value="Dining Room" onclick="window.location.href = 'dining.php'">
				<input class="btn btn-link navbtn" type="button" name="#Kitchen" value="Kitchen" onclick="window.location.href = 'kitchen.php'">
				<input class="btn btn-link navbtn" type="button" name="#Living" value="Living Room" onclick="window.location.href = 'living.php'">
				<input class="btn btn-link navbtn" type="button" name="#Office" value="Office" onclick="window.location.href = 'office.php'">
				<input id="addproduct" class="btn btn-link navbtn" type="button" name="#addproduct" value="ADD PRODUCT" onclick="window.location.href = 'addItem.php'">
			</div>
			
		</div> 	

	</nav>
</div>
<!-- NAVIGATION END -->



		<div class="col-lg-*" style="margin-top: 200px;">
			<h1 class="header text-orange">LOGIN OR CREATE AN ACCOUNT</h1>
		</div>
		<br>

		<div class="container">
			<div class="col-lg-12"><br></div>
			
			<div class="col-lg-6" style="background-color: rgb(213,207,216);padding-bottom: 15px">
				 <?php 

				 if ( $_SESSION['email'] == "no signin" || $_SESSION['user_type'] == "CLIENT") {
				 	
				 	$_SESSION['email'] = "no signin";
				 	$_SESSION['user_type'] = "CLIENT";
				 //	echo "true". $_SESSION['email']. " ".  $_SESSION['user_type'];
				 }
				 else if( $_SESSION['email'] == '' || $_SESSION['user_type'] == ''){
				 	$_SESSION['email'] = "no signin";
				 	$_SESSION['user_type'] = "CLIENT";
				 	echo  "<script> window.location = 'login.php';  </script>";
				 }
				 else{
				 //	echo "false".$_SESSION['email']. " ".  $_SESSION['user_type'];	
				 	echo "<a href='signout.php' >Signout</a>";
					
				 }

					
				?>
				<!-- LOGIN FORM -->
				<form name="f1" method="post" enctype="multipart/form-data" action="signin.php">
					<h3>Login</h3>
					<label>
						Email Address 
					</label>
					<input type="email" name="btEmail" placeholder="E-MAIL OR LOGIN" class="form-control" required="yes" title="Email Address">
					<br>
					<label>
						Password 
					</label>
					<input type="password" name="btPassword" placeholder="PASSWORD" class="form-control" required="yes" title="Password">
					<br>

					<input type="submit" name="btLogin" class="submit-rev btn btn-default col-lg-2" style="width: 150px; height: 50px;" value="Login" title="Login" >
					<input type="button" name="btforgetpass" class="col-lg-2 btn btn-link" style="color: black;width: 200px" value="Forget Your Password ?" align="right"> 
					<br>
				</form>
			</div>
			<!-- REGISTER FORM -->
			<div class="col-lg-6" style="padding-left: 40px">
				<form name="f2" method="post" enctype="multipart/form-data" action="signup.php">
				<h3 style="color: white">Sign up</h3>
				<!-- FULL NAME -->
					<div class="col-lg-12">
						<label style="color: white">
							Full Name <span style="color:red">*</span>
						</label>
						
						<input type="text" name="btFullName" placeholder="" class="form-control" required="yes" title="Full Name">
						<br>		
					</div>
					
					<!-- Email -->		
					<div class="col-lg-6">
						<label style="color: white">
							Email Address <span style="color:red">*</span>
						</label>
						<input type="email" name="btEmailAddress" placeholder="" class="form-control" required="yes" title="Email Address">
						<br>
					</div>
					<!-- Password -->
					<div class="col-lg-6">
						<label style="color: white">
						Password <span style="color:red">*</span>
						</label>
						<input type="password" name="btpass" placeholder="" class="form-control" required="yes" title="Password">
					<br>		
					</div>

					<br>
					<br>
					<br>
					<input type="submit" name="btRegister/Sign Up" class="submit btn btn-default col-lg-2" style="width: 300px; height: 60px;" value="Register/Sign up" title="Create An Account">
				</form>
			</div>
		</div>

		<div class="col-lg-12">
				<br>
				<br>
		</div>
			
	<!-- Stay  -->
		<div class="container-fluid" style=" padding: 0px;">
			
			<div class="col-lg-12" style="background-image: url(imgs/stay.png);margin-bottom: 0px">
				<div class="col-lg-12">
					
				</div>
				<center>
					<h1 style="color: white; font:sans-serif;padding-top: 30px;">
						Stay Updated
					</h1>
					<h2 style="color: white">
						Subscribe to our newsletter for latest updates & discount deals
					</h2>
					<br>
					<form>
						<input type="text" name="txtEmailStay" class="emailstyle"  title="Sign Up For Our Newsletter" placeholder="Enter Email Address">
						<input type="button" name="btnStay" class="text-orange signupstyle " value="SIGN UP"  title="Go">
					</form>
					<br>
					<br>
				</center>
			</div>
		</div>
	<!-- Footer  -->
<div class="container-fluid bg-footer" >
		<div  class="panel panel-default">
			
				<div  class="col-lg-4 " align="left" >
					<div>
						<h3>Payment Methods</h3>
					</div>
					<div>
						<img src="imgs/footerimgs/easypay.png" width="150">
						<img src="imgs/footerimgs/visacard.png" width="100">
					</div>
					<br>
					<div>
						<h4>Get To Know Us</h4>
					
						<p>NTN Number: 7255476-0</p>
					</div>
				</div>

				<div class="col-lg-4" align="left">
					<div>
						<h3 style="padding-left: 10px;">Popular Categories</h3>
					</div>
					<div>
						<input type="button" name="bbedroom" class="btn btn-link button" value="Bedroom" style="font-size: 15pt" onclick="window.location.href = 'bedroom.php'">|
						<input type="button" name="blivingroom" class="btn btn-link button" value="Living Room" style="font-size: 15pt" >|
						<input type="button" name="bkitchen" class="btn btn-link button" value="Kitchen" style="font-size: 15pt"  >|
						<br>
						<input type="button" name="bdiningroom" class="btn btn-link button" value="Dining Room" style="font-size: 15pt" >|
						<input type="button" name="boffice" class="btn btn-link button" value="Office" style="font-size: 15pt" >|
					</div>	
					</div>

					<div class="col-lg-4 " align="right" >
					<div align="center">
						<h3>Information</h3>
					</div>
					<div class="text-center">
						<input type="button" name="bcontact" class="btn btn-link button" value="Contact Us" style="font-size: 15pt" onclick="window.location.href = 'contactUs.php'"><br>
						<input type="button" name="bfaq" class="btn btn-link button" value="FAQ" style="font-size: 15pt"  onclick="window.location.href = 'FAQs.php'"><br>
						<input type="button" name="baboutus" class="btn btn-link button" value="About Us" style="font-size: 15pt" onclick="window.location.href = 'AboutUs.php'"><br>
						<input type="button" name="bDelivery" class="btn btn-link button" value="Delivery & Return" style="font-size: 15pt"  onclick="window.location.href = 'deliveryAndReturn.php'"><br>
						<input type="button" name="bprivacypolicy" class="btn btn-link button" value="Privacy Policy" style="font-size: 15pt"  onclick="window.location.href = 'privacyPolicy.php'">
					</div>
					</div>
				
			</div>
			

			<div class="col-lg-12">
				<div class="col-lg-6 text-right ">
					<p>Email : <span class="text-orange"> customer.care@sam.com </span> </p>
				</div>
				<div class="col-lg-6 ">
					<p>Phone : <span class="text-orange">+92 399 999 9999</span></p>
				</div>
			</div>
					
				<div style="text-align:center;" class="col-lg-12">
					<hr style="width: 100%">
							
						<div style="padding-top: 5px; margin-bottom: -20px;">
							<img src="imgs/footerimgs/face.png" width="50" style="padding-right: 10px;">
							<img src="imgs/footerimgs/pin.png" width="50" style="padding-right: 10px;">
							<img src="imgs/footerimgs/twitter.png" width="50" style="padding-right: 10px;">
						</div>
					
					<hr style="width: 100%">
				</div>
			<div class="col-lg-12">
				<center>
					<p>&copy;AMS Furniture's</p>
				</center>
			</div>
	</div>
	
		



			



</body>
</html>