<!DOCTYPE html>
<html>
<head>
	<title>SAM - Registered Clients</title>
			<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" type="text/css" href="proto2Style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <script type="text/javascript" language="javascript" src="proto2JS.js"></script>
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>



</head>
<body id="bg-body">


<!--NAVIGATION PANEL -->
<div class="container">

	<nav class="navbar-fixed-top" >
		<div class="container-fluid" style="padding-top: 30px;" >
			<div class="row" >
				<!-- LOGO -->
				<div class="col-sm-2" style="padding-left: 50px">
					<a href="home.php"><img src="imgs/logo/sam.png" width="100px" ></a>
				</div>
				<div class="col-sm-3" > <!-- SEARCH BAR -->
					<div class="input-group" style="padding-left: 50px; ">
						<input type="text" name="searchBar" placeholder="Search.." class="form-control">
						<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					</div>
				</div>
				<div class="col-sm-4"></div>
					<!-- loginSignup -->
				<div class="col-sm-3" style="font-size: 12pt; font-family: serif;" >
						<div style="margin-left: -180px;display: inline-block;">
							<span class="glyphicon glyphicon-earphone text-muted" ></span>
							<label class="navlbl" style="" >0399-9999999</label>
						</div>
							
						<div style="display: inline-block;">
							<a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="adminpanel.php"> ACCOUNT </label></a>
						</div>
						<div style="display: inline-block;">
							<a href="signout.php" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="signout.php"> Sign Out </label></a>
						</div>
							
						<div style="display: inline-block;">
							<a href="cart.php" ><span class="glyphicon glyphicon-shopping-cart text-orange" ></span></a>
							<label class="navlbl"><a class="text-orange" href="cart.php"> My Cart </label></a>
						</div>
						
						<!--info-->

						<div class="col-sm-*" >
							<label class="text-orange" style="margin-bottom: 0px;">CASH ON DELIVERY | EASY PAY</label></br>
							<label style= "font-size: 12pt">Product Inquries/Checkout issues? Call Us</label>
						</div>
				</div>
					
			</div>
				
			<!-- PRIMARY MENU -->
			<div class="col-sm-12 prinav text-center" style="padding-bottom:10px  ;margin-right: 0px;margin-top: 20px; background-color: transparent;">
				<input class="btn btn-link navbtn" type="button" name="#BedRoom" value="Bed Room" onclick="window.location.href = 'bedroom.php'">
				<input class="btn btn-link navbtn" type="button" name="#Dining" value="Dining Room" onclick="window.location.href = 'dining.php'">
				<input class="btn btn-link navbtn" type="button" name="#Kitchen" value="Kitchen" onclick="window.location.href = 'kitchen.php'">
				<input class="btn btn-link navbtn" type="button" name="#Living" value="Living Room" onclick="window.location.href = 'living.php'">
				<input class="btn btn-link navbtn" type="button" name="#Office" value="Office" onclick="window.location.href = 'office.php'">
				<input class="btn btn-link navbtn" type="button" name="#addproduct" value="ADD PRODUCT" onclick="window.location.href = 'addItem.php'">
			</div>
			
		</div> 	

	</nav>
</div>	

<!-- NAVIGATION END -->

<div class="container" style="font-family: serif; margin-top: 200px; font-size: 15px;">
	<div class="col-sm-*">
		<a href="adminpanel.php">
          <span class="glyphicon glyphicon-chevron-left"></span> BACK
        </a>
        <br><br>
	</div>
	<div class="col-sm-4 input-group">
	  <input class="form-control" id="myInput" type="text" placeholder="Search in the table">
	  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
	</div>
		
	<div class="col-sm-12">
		<br>
		<table class="table table-responsive bg-theme" border="3">
			<tr class="" style="">
				<th>User Name</th>
				<th>Email</th>
				<th>User Type</th>
			</tr>
			<tbody id="myTable">
				
			<?php
				$conn = mysqli_connect('localhost', 'root','','dummy');
				
				$query = "SELECT * FROM users ORDER BY USER_TYPE";
				
				$res = mysqli_query($conn, $query);
				while ($row = mysqli_fetch_array($res)) {
					echo "
						<tr class=''>
							<td>".$row['USER_NAME']." </td>
							<td> ".$row['EMAIL']."</td>
							<td>".$row['USER_TYPE']." </td>
						</tr>";	
				}

			?>

			</tbody>
		</table>


	</div>
	
</div>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>


</body>
</html>