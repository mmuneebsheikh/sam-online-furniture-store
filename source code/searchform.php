<!DOCTYPE html>
<html>
<head>
	<title>SAM - Search Product</title>
     <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="proto2Style.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript" language="javascript" src="proto2JS.js"></script>
</head>
<body id="bg-body">

<!--NAVIGATION PANEL -->
<div class="container">

	<nav class="navbar-fixed-top" >
		<div class="container-fluid" style="padding-top: 30px;" >
			<div class="row" >
				<!-- LOGO -->
				<div class="col-sm-2" style="padding-left: 50px">
					<a href="home.php"><img src="imgs/logo/sam.png" width="100px" ></a>
				</div>
				<div class="col-sm-3" > <!-- SEARCH BAR -->
					<div class="input-group" style="padding-left: 50px; ">
						<input type="text" name="searchBar" placeholder="Search.." class="form-control">
						<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					</div>
				</div>
				<div class="col-sm-4"></div>
					<!-- loginSignup -->
				<div class="col-sm-3" style="font-size: 12pt; font-family: serif;" >
						<div style="margin-left: -180px;display: inline-block;">
							<span class="glyphicon glyphicon-earphone text-muted" ></span>
							<label class="navlbl" style="" >0399-9999999</label>
						</div>
							
						<div style="display: inline-block;">
							<a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="adminpanel.php"> ACCOUNT </label></a>
						</div>
						<div style="display: inline-block;">
							<a href="signout.php" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="signout.php"> Sign Out </label></a>
						</div>
							
						<div style="display: inline-block;">
							<a href="cart.php" ><span class="glyphicon glyphicon-shopping-cart text-orange" ></span></a>
							<label class="navlbl"><a class="text-orange" href="cart.php"> My Cart </label></a>
						</div>
						
						<!--info-->

						<div class="col-sm-*" >
							<label class="text-orange" style="margin-bottom: 0px;">CASH ON DELIVERY | EASY PAY</label></br>
							<label style= "font-size: 12pt">Product Inquries/Checkout issues? Call Us</label>
						</div>
				</div>
					
			</div>
				
			<!-- PRIMARY MENU -->
			<div class="col-sm-12 prinav text-center" style="padding-bottom:10px  ;margin-right: 0px;margin-top: 20px; background-color: transparent;">
				<input class="btn btn-link navbtn" type="button" name="#BedRoom" value="Bed Room" onclick="window.location.href = 'bedroom.php'">
				<input class="btn btn-link navbtn" type="button" name="#Dining" value="Dining Room" onclick="window.location.href = 'dining.php'">
				<input class="btn btn-link navbtn" type="button" name="#Kitchen" value="Kitchen" onclick="window.location.href = 'kitchen.php'">
				<input class="btn btn-link navbtn" type="button" name="#Living" value="Living Room" onclick="window.location.href = 'living.php'">
				<input class="btn btn-link navbtn" type="button" name="#Office" value="Office" onclick="window.location.href = 'office.php'">
				<input class="btn btn-link navbtn" type="button" name="#addproduct" value="ADD PRODUCT" onclick="window.location.href = 'addItem.php'">
			</div>
			
		</div> 	

	</nav>
</div>	

<!-- NAVIGATION END -->




<!-- SEARCH FORM -->

<div style="margin-top:200px; ">
	
<form method="post" action="searchform.php" enctype="multipart/form-data" class="form-horizontal">
	<fieldset>
		<!--FORM NAME -->
		<legend style="color:darkorange;" class="text-center">SEARCH PRODUCTS</legend>
		
	
		<div class="form-group">
		  <label class="col-md-4 control-label" for="product_id">PRODUCT ID</label>  
		  <div class="col-md-4">
		    <input id="product_id" name="product_id" placeholder="PRODUCT ID" class="form-control input-md" required=""  type="text">
		  </div>
		</div>


		<div class="form-group">
		  <label class="col-md-4 control-label" for="product_categorie">PRODUCT CATEGORY</label>
		  <div class="col-md-4">
		    <select id="product_categorie" name="product_categorie" class="form-control" required="">
		      <option selected="selected" disabled hidden>Select Category</option>
		      <option value="BED_ROOM">Bed Room</option>
		      <option value="DINING_ROOM">Dining Room</option>
		      <option value="KITCHEN">Kitchen</option>
		      <option value="LIVING_ROOM">Living Room</option>
		      <option value="OFFICE_ROOM">Office Room</option>
		      <option value="SPECIAL_OFFER" disabled>Special Offer(currently disabled)</option>
		    </select>
		  </div>
		</div>

		<hr>




		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="product_name">PRODUCT NAME</label>  
		  <div class="col-md-4">
		  <input id="product_name" name="product_name" placeholder="PRODUCT NAME" class="form-control input-md"  type="text">
		    
		  </div>
		</div>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="prod_price">PRODUCT PRICE</label>  
		  <div class="col-md-4">
		    <input id="product_price" name="product_price" placeholder="PRICE" class="form-control input-md"  type="number">
		  </div>
		</div>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="product_name_fr">PRODUCT SHORT DESCRIPTION</label>  
		  <div class="col-md-4">
		  <input id="product_name_fr" name="product_name_fr" placeholder="PRODUCT SHORT DESCRIPTION" class="form-control input-md"  type="text">
		    
		  </div>
		</div>


		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="available_quantity">AVAILABLE QUANTITY</label>  
		  <div class="col-md-4">
		  <input id="available_quantity" name="available_quantity" placeholder="AVAILABLE QUANTITY" class="form-control input-md"  type="text">
		    
		  </div>
		</div>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="product_weight">PRODUCT WEIGHT</label>  
		  <div class="col-md-4">
		  <input id="product_weight" name="product_weight" placeholder="PRODUCT WEIGHT" class="form-control input-md"  type="number">
		    
		  </div>
		</div>    

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="author">COMPANY</label>  
		  <div class="col-md-4">
		  <input id="author" name="author" placeholder="COMPANY" class="form-control input-md"  type="text">
		    
		  </div>
		</div>








<?php

// SEARCH PRODUCT

$conn = mysqli_connect("localhost", "root", "", "dummy");

$prod_id;
$prod_cate;

if (isset($_POST['search'])) {

	$prod_id = $_POST['product_id'];	
	$prod_cate = $_POST['product_categorie'];
	

	// echo $prod_id;

	$query = "SELECT * FROM $prod_cate WHERE ID = '$prod_id'";
	$res = mysqli_query($conn,$query);


	if ($res) {
		if (mysqli_num_rows($res) == 0) {
			echo "<script> alert('No Record was Found'); window.location = 'searchform.php' </script>";
		}
		else{
			$row = mysqli_fetch_array($res);		
			$prod_id = $row['ID'];	
			
			$prod_name = $row['NAME'];
			$prod_price = $row['PRICE'];
			$prod_desc = $row['DESCRIPTION'];
			$prod_qty = $row['QTY'];
			$prod_weight = $row['WEIGHT'];
			$prod_company = $row['COMPANY'];

			//echo "NAME: " .$prod_name. " Price: ".$prod_price;
		echo "<script> 
				$(document).ready(function(){
					$('#product_name').val('".$prod_name."');
					$('#product_price').val('".$prod_price."');
					$('#product_name_fr').val('".$prod_desc."');
					$('#available_quantity').val('".$prod_qty."');
					$('#product_weight').val('".$prod_weight."');
					$('#author').val('".$prod_company."');
					$('#product_id').val('".$prod_id."');
					$('#product_categorie').val('".$prod_cate."');
					$('#product_id').removeAttr('required');
					$('#product_categorie').removeAttr('required');
				});
			</script>
			<div class='form-group'>
				<div class='col-md-4 text-right'> <label class = 'control-label'>IMAGE</label>  </div>
				<img class='col-md-4' width='200' height='200' src='data:iamge/jpg;base64,".base64_encode($row["IMAGE"])."' /> <br>
				<label class = 'control-label'> new Image </label>
				<input type='file' name='newimage' accept='image/*'/>
			</div>
			";
		}
	}


	else{
		echo "<br>".mysqli_error($conn);
	}



}


// UPDATE PRODUCT

if (isset($_POST['update'])) {



	$cate = $_POST['product_categorie'];
	$id = $_POST['product_id'];
	

	$name = $_POST['product_name'];
	$price = $_POST['product_price'];
	$des = $_POST['product_name_fr'];
	$qty = $_POST['available_quantity'];
	$weight = $_POST['product_weight'];
	$company = $_POST['author'];
	$file = addslashes(file_get_contents($_FILES["newimage"]["tmp_name"]));


	$query = "UPDATE $cate SET NAME = '$name' , PRICE = '$price', DESCRIPTION = '$des', QTY = '$qty', WEIGHT = '$weight', COMPANY = '$company', IMAGE = '$file' WHERE ID = '$id'";
	$res = mysqli_query($conn,$query);
	if ($res) {
		echo "<script> alert('Record Updated Successfully!'); window.location = searchform.php; </script> ";
	}
	else{
		echo "<script> alert('Record Update Failed!".mysqli_error($conn)."'); window.location = searchform.php;   </script>" ;
	}

}


if (isset($_POST['delete'])) {

	$id = $_POST['product_id'];
	$cate = $_POST['product_categorie'];
	$query = "DELETE FROM $cate WHERE ID = '$id'";
	$res = mysqli_query($conn, $query);
	if ($res) {
		echo "<script> alert('Product Is Deleted!'); window.location = 'searchform.php'; </script>" ;
	}
	else{
		echo mysqli_error($conn);
		// echo"<script> alert('Unable to Delete Product ".."' ); window.location = 'searchform.php'; </script>";
	}


}


?>


		<!-- Button -->
		<div class="container" align="right">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
			  <!-- <label class="col-md-4 control-label" for="singlebutton">Publish</label> -->
			    <input id="singlebutton" name="search" class="submit-rev btn btn-default col-sm-4" type="submit" value="Search">
			    <input id="singlebutton" name="update" class="submit-rev btn btn-default col-sm-4" type="submit" value="Update">
			    <input id="singlebutton" name="delete" class="submit-rev btn btn-default col-sm-4" type="submit" value="Delete">
			    <!-- <button id="singlebutton" name="search" class="submit-rev btn btn-default"> Search</button> -->
			    <!-- <button id="singlebutton" name="update" class="btn btn-primary"> Update</button>
			    <button id="singlebutton" name="delete" class="btn btn-primary"> Delete</button> -->
			</div>
			    
		</div>

	</fieldset>
</form>
			<br><br>
</div>


	<!-- Footer  -->
<div class="container-fluid bg-footer" >
		<div  class="panel panel-default">
			
				<div  class="col-lg-4 " align="left" >
					<div>
						<h3>Payment Methods</h3>
					</div>
					<div>
						<img src="imgs/footerimgs/easypay.png" width="150">
						<img src="imgs/footerimgs/visacard.png" width="100">
					</div>
					<br>
					<div>
						<h4>Get To Know Us</h4>
					
						<p>NTN Number: 7255476-0</p>
					</div>
				</div>

				<div class="col-lg-4" align="left">
					<div>
						<h3 style="padding-left: 10px;">Popular Categories</h3>
					</div>
					<div>
						<input type="button" name="bbedroom" class="btn btn-link button" value="Bedroom" style="font-size: 15pt" onclick="window.location.href = 'bedroom.php'">|
						<input type="button" name="blivingroom" class="btn btn-link button" value="Living Room" style="font-size: 15pt" >|
						<input type="button" name="bkitchen" class="btn btn-link button" value="Kitchen" style="font-size: 15pt"  >|
						<br>
						<input type="button" name="bdiningroom" class="btn btn-link button" value="Dining Room" style="font-size: 15pt" >|
						<input type="button" name="boffice" class="btn btn-link button" value="Office" style="font-size: 15pt" >|
					</div>	
					</div>

					<div class="col-lg-4 " align="right" >
					<div align="center">
						<h3>Information</h3>
					</div>
					<div class="text-center">
						<input type="button" name="bcontact" class="btn btn-link button" value="Contact Us" style="font-size: 15pt" onclick="window.location.href = 'contactUs.php'"><br>
						<input type="button" name="bfaq" class="btn btn-link button" value="FAQ" style="font-size: 15pt"  onclick="window.location.href = 'FAQs.php'"><br>
						<input type="button" name="baboutus" class="btn btn-link button" value="About Us" style="font-size: 15pt" onclick="window.location.href = 'AboutUs.php'"><br>
						<input type="button" name="bDelivery" class="btn btn-link button" value="Delivery & Return" style="font-size: 15pt"  onclick="window.location.href = 'deliveryAndReturn.php'"><br>
						<input type="button" name="bprivacypolicy" class="btn btn-link button" value="Privacy Policy" style="font-size: 15pt"  onclick="window.location.href = 'privacyPolicy.php'">
					</div>
					</div>
				
			</div>
			

			<div class="col-lg-12">
				<div class="col-lg-6 text-right ">
					<p>Email : <span class="text-orange"> customer.care@sam.com </span> </p>
				</div>
				<div class="col-lg-6 ">
					<p>Phone : <span class="text-orange">+92 399 999 9999</span></p>
				</div>
			</div>
					
				<div style="text-align:center;" class="col-lg-12">
					<hr style="width: 100%">
							
						<div style="padding-top: 5px; margin-bottom: -20px;">
							<img src="imgs/footerimgs/face.png" width="50" style="padding-right: 10px;">
							<img src="imgs/footerimgs/pin.png" width="50" style="padding-right: 10px;">
							<img src="imgs/footerimgs/twitter.png" width="50" style="padding-right: 10px;">
						</div>
					
					<hr style="width: 100%">
				</div>
			<div class="col-lg-12">
				<center>
					<p>&copy;AMS Furniture's</p>
				</center>
			</div>
	</div>




</body>
</html>
