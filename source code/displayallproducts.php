<!DOCTYPE html>
<html>
<head>
	<title>SAM - ALL INVENTORY</title>
		<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" type="text/css" href="proto2Style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <script type="text/javascript" language="javascript" src="proto2JS.js"></script>

</head>
<body id="bg-body">


<!--NAVIGATION PANEL -->
<div class="container">

	<nav class="navbar-fixed-top" >
		<div class="container-fluid" style="padding-top: 30px;" >
			<div class="row" >
				<!-- LOGO -->
				<div class="col-sm-2" style="padding-left: 50px">
					<a href="home.php"><img src="imgs/logo/sam.png" width="100px" ></a>
				</div>
				<div class="col-sm-3" > <!-- SEARCH BAR -->
					<div class="input-group" style="padding-left: 50px; ">
						<input type="text" name="searchBar" placeholder="Search.." class="form-control">
						<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					</div>
				</div>
				<div class="col-sm-4"></div>
					<!-- loginSignup -->
				<div class="col-sm-3" style="font-size: 12pt; font-family: serif;" >
						<div style="margin-left: -180px;display: inline-block;">
							<span class="glyphicon glyphicon-earphone text-muted" ></span>
							<label class="navlbl" style="" >0399-9999999</label>
						</div>
							
						<div style="display: inline-block;">
							<a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="adminpanel.php"> ACCOUNT </label></a>
						</div>
						<div style="display: inline-block;">
							<a href="signout.php" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="signout.php"> Sign Out </label></a>
						</div>
							
						<div style="display: inline-block;">
							<a href="cart.php" ><span class="glyphicon glyphicon-shopping-cart text-orange" ></span></a>
							<label class="navlbl"><a class="text-orange" href="cart.php"> My Cart </label></a>
						</div>

						<!--info-->

						<div class="col-sm-*" >
							<label class="text-orange" style="margin-bottom: 0px;">CASH ON DELIVERY | EASY PAY</label></br>
							<label style= "font-size: 12pt">Product Inquries/Checkout issues? Call Us</label>
						</div>
				</div>
					
			</div>
				
			<!-- PRIMARY MENU -->
			<div class="col-sm-12 prinav text-center" style="padding-bottom:10px  ;margin-right: 0px;margin-top: 20px; background-color: transparent;">
				<input class="btn btn-link navbtn" type="button" name="#BedRoom" value="Bed Room" onclick="window.location.href = 'bedroom.php'">
				<input class="btn btn-link navbtn" type="button" name="#Dining" value="Dining Room" onclick="window.location.href = 'dining.php'">
				<input class="btn btn-link navbtn" type="button" name="#Kitchen" value="Kitchen" onclick="window.location.href = 'kitchen.php'">
				<input class="btn btn-link navbtn" type="button" name="#Living" value="Living Room" onclick="window.location.href = 'living.php'">
				<input class="btn btn-link navbtn" type="button" name="#Office" value="Office" onclick="window.location.href = 'office.php'">
				<input class="btn btn-link navbtn" type="button" name="#addproduct" value="ADD PRODUCT" onclick="window.location.href = 'addItem.php'">
			</div>
			
		</div> 	

	</nav>
</div>	

<!-- NAVIGATION END -->


<div class="container-fluid text-center" style="font-family: serif; margin-top: 200px; ">
	<div class="container" style="text-align: left;">
		<a href="adminpanel.php">
          <span class="glyphicon glyphicon-chevron-left"></span> BACK
        </a>
        <br>
	</div>
	<!--ROW 1 -->
	<div class="row">
		<!-- BED ROOM -->	
		<div class="col-md-6">
			<h3 class="text-center text-orange">Bed Room Products</h3>
				<div class="col-sm-4 input-group">
				  <input class="form-control" id="myInput" type="text" placeholder="Search in the table">
				  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
				</div>
				<br>
		<table class="table table-responsive bg-theme " border="3">
				
			<tr>
				<th>ID</th>
				<th>NAME</th>
				<th>DESCRIPTION</th>
				<th>QUANTITY</th>
				<th>PRICE</th>
				<th>IMAGE</th>
			</tr>
			
			<tbody id="myTable">
				
				<?php
					$conn = mysqli_connect('localhost', 'root','', 'dummy');

					$query = "SELECT * FROM bed_room";
					$res = mysqli_query($conn, $query);
					while ($row = mysqli_fetch_array($res)) {
						echo "
						<tr>
							<td>".$row['ID']."</td>
							<td>".$row['NAME']."</td>
							<td>".$row['DESCRIPTION']."</td>
							<td>".$row['QTY']."</td>
							<td>".$row['PRICE']."</td>
							<td><img height = '100' width='200'  src='data:image/jpg;base64,".base64_encode($row["IMAGE"])."' ></td>
						</tr>";
					}


				?>
			</tbody>
		</table>
		</div>


		<!-- KITCHEN ROOM -->	
		<div class="col-md-6">
			<h3 class="text-center text-orange">KITCHEN Products</h3>
			<div class="col-sm-4 input-group">
			  <input class="form-control" id="myInputkit" type="text" placeholder="Search in the table">
			  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
			</div>
				<br>
		<table class="table table-responsive bg-theme " border="3">
				
			<tr>
				<th>ID</th>
				<th>NAME</th>
				<th>DESCRIPTION</th>
				<th>QUANTITY</th>
				<th>PRICE</th>
				<th>IMAGE</th>
			</tr>
			
			<tbody id="myTablekit">
				
			<?php
				$conn = mysqli_connect('localhost', 'root','', 'dummy');

				$query = "SELECT * FROM kitchen";
				$res = mysqli_query($conn, $query);
				while ($row = mysqli_fetch_array($res)) {
					echo "
					<tr>
						<td>".$row['ID']."</td>
						<td>".$row['NAME']."</td>
						<td>".$row['DESCRIPTION']."</td>
						<td>".$row['QTY']."</td>
						<td>".$row['PRICE']."</td>
						<td><img height = '100' width='200'  src='data:image/jpg;base64,".base64_encode($row["IMAGE"])."' ></td>
					</tr>";
				}


			?>
			</tbody>
		</table>
		</div>
	</div> 	<!-- ROW 1 end-->

	<br> <br>
	<!-- ROW 2-->
	<div class="row">
		
				<!-- DINING ROOM -->	
		<div class="col-md-6">
			<h3 class="text-center text-orange">Dining Room Products</h3>
			<div class="col-sm-4 input-group">
			  <input class="form-control" id="myInputdin" type="text" placeholder="Search in the table">
			  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
			</div>
				<br>
		<table class="table table-responsive bg-theme " border="3">
				
			<tr>
				<th>ID</th>
				<th>NAME</th>
				<th>DESCRIPTION</th>
				<th>QUANTITY</th>
				<th>PRICE</th>
				<th>IMAGE</th>
			</tr>
			<tbody id="myTabledin">
				

			<?php
				$conn = mysqli_connect('localhost', 'root','', 'dummy');

				$query = "SELECT * FROM dining_room";
				$res = mysqli_query($conn, $query);
				while ($row = mysqli_fetch_array($res)) {
					echo "
					<tr>
						<td>".$row['ID']."</td>
						<td>".$row['NAME']."</td>
						<td>".$row['DESCRIPTION']."</td>
						<td>".$row['QTY']."</td>
						<td>".$row['PRICE']."</td>
						<td><img height = '100' width='200'  src='data:image/jpg;base64,".base64_encode($row["IMAGE"])."' ></td>
					</tr>";
				}


			?>
			</tbody>
		</table>
		</div>

				<!-- LIVING ROOM -->	
		<div class="col-md-6">
			<h3 class="text-center text-orange">LIVING Room Products</h3>
			<div class="col-sm-4 input-group">
			  <input class="form-control" id="myInputliv" type="text" placeholder="Search in the table">
			  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
			</div>
				<br>
		<table class="table table-responsive bg-theme " border="3">
				
			<tr>
				<th>ID</th>
				<th>NAME</th>
				<th>DESCRIPTION</th>
				<th>QUANTITY</th>
				<th>PRICE</th>
				<th>IMAGE</th>
			</tr>
			
			<tbody id="myTableliv">
				
			<?php
				$conn = mysqli_connect('localhost', 'root','', 'dummy');

				$query = "SELECT * FROM living_room";
				$res = mysqli_query($conn, $query);
				while ($row = mysqli_fetch_array($res)) {
					echo "
					<tr>
						<td>".$row['ID']."</td>
						<td>".$row['NAME']."</td>
						<td>".$row['DESCRIPTION']."</td>
						<td>".$row['QTY']."</td>
						<td>".$row['PRICE']."</td>
						<td><img height = '100' width='200'  src='data:image/jpg;base64,".base64_encode($row["IMAGE"])."' ></td>
					</tr>";
				}


			?>
			</tbody>
		</table>
		</div>
	</div> <!-- row2 end -->		
		


</div>


<br><br>



	<!-- Footer  -->
<div class="container-fluid bg-footer" >
		<div  class="panel panel-default">
			
				<div  class="col-lg-4 " align="left" >
					<div>
						<h3>Payment Methods</h3>
					</div>
					<div>
						<img src="imgs/footerimgs/easypay.png" width="150">
						<img src="imgs/footerimgs/visacard.png" width="100">
					</div>
					<br>
					<div>
						<h4>Get To Know Us</h4>
					
						<p>NTN Number: 7255476-0</p>
					</div>
				</div>

				<div class="col-lg-4" align="left">
					<div>
						<h3 style="padding-left: 10px;">Popular Categories</h3>
					</div>
					<div>
						<input type="button" name="bbedroom" class="btn btn-link button" value="Bedroom" style="font-size: 15pt" onclick="window.location.href = 'bedroom.php'">|
						<input type="button" name="blivingroom" class="btn btn-link button" value="Living Room" style="font-size: 15pt" >|
						<input type="button" name="bkitchen" class="btn btn-link button" value="Kitchen" style="font-size: 15pt"  >|
						<br>
						<input type="button" name="bdiningroom" class="btn btn-link button" value="Dining Room" style="font-size: 15pt" >|
						<input type="button" name="boffice" class="btn btn-link button" value="Office" style="font-size: 15pt" >|
					</div>	
					</div>

					<div class="col-lg-4 " align="right" >
					<div align="center">
						<h3>Information</h3>
					</div>
					<div class="text-center">
						<input type="button" name="bcontact" class="btn btn-link button" value="Contact Us" style="font-size: 15pt" onclick="window.location.href = 'contactUs.php'"><br>
						<input type="button" name="bfaq" class="btn btn-link button" value="FAQ" style="font-size: 15pt"  onclick="window.location.href = 'FAQs.php'"><br>
						<input type="button" name="baboutus" class="btn btn-link button" value="About Us" style="font-size: 15pt" onclick="window.location.href = 'AboutUs.php'"><br>
						<input type="button" name="bDelivery" class="btn btn-link button" value="Delivery & Return" style="font-size: 15pt"  onclick="window.location.href = 'deliveryAndReturn.php'"><br>
						<input type="button" name="bprivacypolicy" class="btn btn-link button" value="Privacy Policy" style="font-size: 15pt"  onclick="window.location.href = 'privacyPolicy.php'">
					</div>
					</div>
				
			</div>
			

			<div class="col-lg-12">
				<div class="col-lg-6 text-right ">
					<p>Email : <span class="text-orange"> customer.care@sam.com </span> </p>
				</div>
				<div class="col-lg-6 ">
					<p>Phone : <span class="text-orange">+92 399 999 9999</span></p>
				</div>
			</div>
					
				<div style="text-align:center;" class="col-lg-12">
					<hr style="width: 100%">
							
						<div style="padding-top: 5px; margin-bottom: -20px;">
							<img src="imgs/footerimgs/face.png" width="50" style="padding-right: 10px;">
							<img src="imgs/footerimgs/pin.png" width="50" style="padding-right: 10px;">
							<img src="imgs/footerimgs/twitter.png" width="50" style="padding-right: 10px;">
						</div>
					
					<hr style="width: 100%">
				</div>
			<div class="col-lg-12">
				<center>
					<p>&copy;AMS Furniture's</p>
				</center>
			</div>
	</div>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $("#myInputkit").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTablekit tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $("#myInputdin").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTabledin tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $("#myInputliv").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTableliv tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

});
</script>


</body>
</html>