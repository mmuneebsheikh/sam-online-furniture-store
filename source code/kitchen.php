<?php

session_start();

?>
<!DOCTYPE html>
<html>

<head>
	<title>Shop - Kitchen</title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" type="text/css" href="proto2Style.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <script type="text/javascript" language="javascript" src="proto2JS.js"></script>
</head>
<body id="bg-body">
	 <?php 

		include 'clasconnect.php';	 
		 $ses = new CurrentSession();
		 $ses->SessionEffect('kitchen.php');

	?>
	
		<!-- NAVIGATION PANEL -->
<div class="container">

	<nav class="navbar-fixed-top" >
		<div class="container-fluid" style="padding-top: 30px;" >
			<div class="row" >
				<!-- LOGO -->
				<div class="col-sm-2" style="padding-left: 50px">
					<a href="home.php"><img src="imgs/logo/sam.png" width="100px" ></a>
				</div>
				<div class="col-sm-3" > <!-- SEARCH BAR -->
					<div class="input-group" style="padding-left: 50px; ">
						<input type="text" name="searchBar" placeholder="Search.." class="form-control">
						<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					</div>
				</div>
				<div class="col-sm-4"></div>
				<div class="col-sm-3" style="font-size: 12pt; font-family: serif;" >
					<!-- loginSignup -->
					<div style="margin-left: -90px;display: inline-block;">
						<span class="glyphicon glyphicon-earphone text-muted" ></span>
						<label class="navlbl" style="" >0399-9999999</label>
					</div>
						
						<div id="linklogin" style="display: inline-block;">
							<a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="login.php"> Login / Signup</label></a>
						</div>
							
						<div id="clientacc" style="display: inline-block;">
							<a href="signout.php" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="signout.php"> Signout </label></a>
						</div>
							
						<div id="adminacc" style="display: inline-block;">
							<a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
							<label class="navlbl" id="login"><a href="adminpanel.php"> ACCOUNT </label></a>
						</div>
							
						<div style="display: inline-block;">
							<a href="cart.php" ><span class="glyphicon glyphicon-shopping-cart text-orange" ></span></a>
							<label class="navlbl"><a class="text-orange" href="cart.php"> My Cart </label></a>
						</div>
							 
						
						<!--info-->

						<div class="col-sm-*" >
							<label class="text-orange" style="margin-bottom: 0px;">CASH ON DELIVERY | EASY PAY</label></br>
							<label style= "font-size: 12pt">Product Inquries/Checkout issues? Call Us</label>
						</div>
				</div>
					
			</div>
				
			<!-- PRIMARY MENU -->
			<div class="col-sm-12 prinav text-center" style="padding-bottom:10px  ;margin-right: 0px;margin-top: 20px; background-color: transparent;">
				<input class="btn btn-link navbtn" type="button" name="#BedRoom" value="Bed Room" onclick="window.location.href = 'bedroom.php'">
				<input class="btn btn-link navbtn" type="button" name="#Dining" value="Dining Room" onclick="window.location.href = 'dining.php'">
				<input class="btn btn-link navbtn" type="button" name="#Kitchen" value="Kitchen" onclick="window.location.href = 'kitchen.php'">
				<input class="btn btn-link navbtn" type="button" name="#Living" value="Living Room" onclick="window.location.href = 'living.php'">
				<input class="btn btn-link navbtn" type="button" name="#Office" value="Office" onclick="window.location.href = 'office.php'">
				<input id="addproduct" class="btn btn-link navbtn" type="button" name="#addproduct" value="ADD PRODUCT" onclick="window.location.href = 'addItem.php'">
			</div>
			
		</div> 	

	</nav>
</div>	

<!-- NAVIGATION END -->





	
<!-- Categories -->
	<div class="container-fluid" style="margin-top: 200px;">
            <div class="col-sm-3 sidenav" style="background-color: transparent; border-radius: 15px;">
                <p class="lead text-center" id="categories" >CATEGORIES</p>
                <div class="list-group text-center" style="font-family: serif; letter-spacing: 2px; font-size: 15pt;">
                    <a href="bedroom.php" class="list-group-item list-unactive" style="color:silver" >Bedroom</a>
                    <a href="dining.php" class="list-group-item list-unactive" style="color:silver" >Dining Room</a>
                    <a href="living.php" class="list-group-item list-unactive" style="color:silver" >Living Room</a>
                    <a href="kitchen.php" class="list-group-item list-active" >Kitchen</a>
                    <a href="office.php" class="list-group-item list-unactive" style="color:silver" >Office</a>

                </div>
            </div>
		<div class="row"  style="z-index: -1">
				<div class="col-sm-3"></div>
	            <div class="col-sm-9" style="margin-bottom: 20px;">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						
						 <!-- Wrapper for slides -->
						 <div class="carousel-inner">
						 	<div class="item active">
						 		<img src="imgs/pbanner8.png" alt="banner1.jpg" style="width:100%" >
						 	</div>
						 	<div class="item">
						 		<img src="imgs/banner4.jpg" alt="" style="width:100%" >
						 	</div>
						 	<div class="item">
						 		<img src="imgs/banner11.jpg" alt="" style="width:100%" >
						 	</div>		
						 	 <!-- Left and right controls -->
							  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
							    <span class="glyphicon glyphicon-chevron-left"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control" href="#myCarousel" data-slide="next">
							    <span class="glyphicon glyphicon-chevron-right"></span>
							    <span class="sr-only">Next</span>
							  </a>	 	
						 </div>
					</div>
				</div>

				<!--ITEMS -->
				<div class="col-sm-3"></div>
				<div class="col-sm-9">
					
		<?php

		$connect = mysqli_connect("localhost", "root", "", 'dummy'); //connect to databese -> test

				$query = "SELECT * FROM kitchen ORDER BY ID DESC";
				$result = mysqli_query($connect, $query);
				 if(mysqli_num_rows($result) > 0)
				 {
				while ($row = mysqli_fetch_array($result)) {

					?>
					<div class="col-sm-4">
		            <form method="post" action="cart.php?action=add&ID=<?php echo $row["ID"]; ?>">
		            <div class='thumbnail'>
		            	<a href='#'>
		            <?php echo "<img src = 'data:iamge/jpg;base64,".base64_encode($row["IMAGE"])."' height='150' width='300'>"?>
		            <div class="caption">
		            	<h2><?php echo $row["NAME"]; ?></h2>
		            	<h3 style='color:black;'>$ <?php echo $row["PRICE"]; ?></h3>
						<p style='color:black;'><?php echo $row["DESCRIPTION"]; ?></p>
						<p style='color:black;'>weight: <?php echo $row["WEIGHT"]; ?></p>
		           		<p style='color:black;'>company: <?php echo $row["COMPANY"]; ?></p>
		            </div>
		            <input type="number" name="quantity" class="form-control" value="1">
		            <input type="hidden" name="hidden_name" value="<?php echo $row["NAME"]; ?>">
		            <input type="hidden" name="hidden_price" value="<?php echo $row["PRICE"]; ?>">
		            <input type="hidden" name="hidden_des" value="<?php echo $row["DESCRIPTION"]; ?>">
		            <input type="hidden" name="hidden_weight" value="<?php echo $row["WEIGHT"]; ?>">
		            <input type="hidden" name="hidden_company" value="<?php echo $row["COMPANY"]; ?>">
		            
		            <input type="submit" name="add" style="margin-top:5px;" class="btn btn-success" value="Add to Bag">

		            </div>
		        		</a>
		            </form>
		            </div>
					
					<?php
					}
				}
			?>	
		</div>
</body>
</html>