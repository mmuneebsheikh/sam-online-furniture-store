<?php
session_start();
$connect = mysqli_connect("localhost", "root", "", "dummy");
if(isset($_POST["add"]))
{
    if(isset($_SESSION["cart"]))
    {
        $item_array_id = array_column($_SESSION["cart"], "product_id");

        $count = count($_SESSION["cart"]);

            $item_array = array(
            'product_id' => $_GET["ID"],
            'item_name' => $_POST["hidden_name"],
            'product_price' => $_POST["hidden_price"],
            'item_des' => $_POST["hidden_des"],
            'item_weight' => $_POST["hidden_weight"],
            'item_company' => $_POST["hidden_company"],
            'item_quantity' => $_POST["quantity"]
            );


        if(!in_array($_GET["ID"], $item_array_id))
        {

            $_SESSION["cart"][$count] = $item_array;
            echo '<script>window.location="cart.php"</script>';
            
        }
        else
        {  
            echo '<script>alert("Products already added to cart")</script>';
            echo '<script>window.location="home.php"</script>';
        }
        
    }
    else
    {
        $item_array = array(
        'product_id' => $_GET["ID"],
            'item_name' => $_POST["hidden_name"],
            'product_price' => $_POST["hidden_price"],
            'item_des' => $_POST["hidden_des"],
            'item_weight' => $_POST["hidden_weight"],
            'item_company' => $_POST["hidden_company"],
            'item_quantity' => $_POST["quantity"]
        );
        $_SESSION["cart"][0] = $item_array;
    }
}
if(isset($_GET["action"]))
{
    if($_GET["action"] == "delete")
    {

        foreach($_SESSION["cart"] as $keys => $values)
        {
             if($values["product_id"] == $_GET["ID"])
             {
                unset($_SESSION["cart"][$keys]);
                echo '<script>alert("Product has been removed")</script>';
                echo '<script>window.location="cart.php"</script>';
             }
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>MY CART 
    </title>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="proto2Style.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript" language="javascript" src="proto2JS.js"></script>
<body id="bg-body">

     <?php 

        include 'clasconnect.php';   
         $ses = new CurrentSession();
         $ses->SessionEffect('cart.php');

    ?>



        <!-- NAVIGATION PANEL -->
<div class="container">

    <nav class="navbar-fixed-top" >
        <div class="container-fluid" style="padding-top: 30px;" >
            <div class="row" >
                <!-- LOGO -->
                <div class="col-sm-2" style="padding-left: 50px">
                    <a href="home.php"><img src="imgs/logo/sam.png" width="100px" ></a>
                </div>
                <div class="col-sm-3" > <!-- SEARCH BAR -->
                    <div class="input-group" style="padding-left: 50px; ">
                        <input type="text" name="searchBar" placeholder="Search.." class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                    </div>
                </div>

                <div class="col-sm-4"></div>
                <div class="col-sm-3" style="font-size: 12pt; font-family: serif;" >
                    <!-- loginSignup -->
                    <div style="margin-left: -90px;display: inline-block;">
                        <span class="glyphicon glyphicon-earphone text-muted" ></span>
                        <label class="navlbl" style="" >0399-9999999</label>
                    </div>
                        
                        <div id="linklogin" style="display: inline-block;">
                            <a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
                            <label class="navlbl" id="login"><a href="login.php"> Login / Signup</label></a>
                        </div>
                            
                        <div id="clientacc" style="display: inline-block;">
                            <a href="signout.php" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
                            <label class="navlbl" id="login"><a href="signout.php"> Signout </label></a>
                        </div>
                            
                        <div id="adminacc" style="display: inline-block;">
                            <a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
                            <label class="navlbl" id="login"><a href="adminpanel.php"> ACCOUNT </label></a>
                        </div>
                            
                        <div style="display: inline-block;">
                            <a href="cart.php" ><span class="glyphicon glyphicon-shopping-cart text-orange" ></span></a>
                            <label class="navlbl"><a class="text-orange" href="cart.php"> My Cart </label></a>
                        </div>
                             
                            
                        
                        <!--info-->

                        <div class="col-sm-*" >
                            <label class="text-orange" style="margin-bottom: 0px;">CASH ON DELIVERY | EASY PAY</label></br>
                            <label style= "font-size: 12pt">Product Inquries/Checkout issues? Call Us</label>
                        </div>
                </div>
                    
            </div>
                
        <!-- PRIMARY MENU -->
            <div class="col-sm-12 prinav text-center" style="padding-bottom:10px  ;margin-right: 0px;margin-top: 20px; background-color: transparent;">
                <input class="btn btn-link navbtn" type="button" name="#BedRoom" value="Bed Room" onclick="window.location.href = 'bedroom.php'">
                <input class="btn btn-link navbtn" type="button" name="#Dining" value="Dining Room" onclick="window.location.href = 'dining.php'">
                <input class="btn btn-link navbtn" type="button" name="#Kitchen" value="Kitchen" onclick="window.location.href = 'kitchen.php'">
                <input class="btn btn-link navbtn" type="button" name="#Living" value="Living Room" onclick="window.location.href = 'living.php'">
                <input class="btn btn-link navbtn" type="button" name="#Office" value="Office" onclick="window.location.href = 'office.php'">
                <input id="addproduct" class="btn btn-link navbtn" type="button" name="#addproduct" value="ADD PRODUCT" onclick="window.location.href = 'addItem.php'">
            </div>
            
        </div>  

    </nav>
</div>
<!-- NAVIGATION END -->
    <div class="container" style="margin-top: 200px;">
        

        <h2 class="text-orange" style="font-family: serif;">  <blockquote class="bg-theme" style="font-size: 30px;" >MY CART</blockquote> </h2>
        <div class="table-responsive">
        <table class="table table-bordered">
        <tr>
        <th width="20%">Product Name</th>
        <th width="10%">Quantity</th>
        <th width="10%">Price Details</th>
        <th width="50%" >Description</th>
        <th width="10">Weight</th>
        <th width="30">Company</th>
        <th width="15%">Order Total</th>
        <th width="5%">Action</th>
        </tr>
        <?php
        if(!empty($_SESSION["cart"]))
        {
            $total = 0;
            foreach($_SESSION["cart"] as $keys => $values)
            {
                ?>
                <tr>
                <td><?php echo $values["item_name"]; ?></td>
                <td><?php echo $values["item_quantity"] ?></td>
                <td>$ <?php echo $values["product_price"]; ?></td>
                <td><?php echo $values["item_des"]; ?></td>
                <td><?php echo $values["item_weight"]; ?> KG</td>
                <td><?php echo $values["item_company"]; ?></td>
                <td>$ <?php echo number_format($values["item_quantity"] * $values["product_price"], 2); ?></td>
                <td><a href="cart.php?action=delete&ID=<?php echo $values["product_id"]; ?>"><span class="text-danger">Remove</span></a></td>
                
                </tr>
                
                <?php 
                $total = $total + ($values["item_quantity"] * $values["product_price"]);
            }
            ?>
            <tr>
            <td colspan="6" align="right">Total</td>
            <td align="right">$ <?php echo number_format($total, 2); ?></td>
            <td></td>
            </tr>

            <?php
        }
        ?>
        </table>
                <input class="submit-rev btn btn-default col-lg-3" type="button" name="Checkout" value="Checkout" onclick="window.location.href='https://www.paypal.com/signin?country.x=US&locale.x=en_US'" >
                
        </div>
    </div>
                <br><br>
</body>

<footer>
        <!-- Footer  -->
<div class="container-fluid bg-footer" >
        <div  class="panel panel-default">
            
                <div  class="col-lg-4 " align="left" >
                    <div>
                        <h3>Payment Methods</h3>
                    </div>
                    <div>
                        <img src="imgs/footerimgs/easypay.png" width="150">
                        <img src="imgs/footerimgs/visacard.png" width="100">
                    </div>
                    <br>
                    <div>
                        <h4>Get To Know Us</h4>
                    
                        <p>NTN Number: 7255476-0</p>
                    </div>
                </div>

                <div class="col-lg-4" align="left">
                    <div>
                        <h3 style="padding-left: 10px;">Popular Categories</h3>
                    </div>
                    <div>
                        <input type="button" name="bbedroom" class="btn btn-link button" value="Bedroom" style="font-size: 15pt" onclick="window.location.href = 'bedroom.html'">|
                        <input type="button" name="blivingroom" class="btn btn-link button" value="Living Room" style="font-size: 15pt" >|
                        <input type="button" name="bkitchen" class="btn btn-link button" value="Kitchen" style="font-size: 15pt"  >|
                        <br>
                        <input type="button" name="bdiningroom" class="btn btn-link button" value="Dining Room" style="font-size: 15pt" >|
                        <input type="button" name="boffice" class="btn btn-link button" value="Office" style="font-size: 15pt" >|
                    </div>  
                    </div>

                    <div class="col-lg-4 " align="right" >
                    <div align="center">
                        <h3>Information</h3>
                    </div>
                    <div class="text-center">
                        <input type="button" name="bcontact" class="btn btn-link button" value="Contact Us" style="font-size: 15pt" onclick="window.location.href = 'contactUs.php'"><br>
                        <input type="button" name="bfaq" class="btn btn-link button" value="FAQ" style="font-size: 15pt"  onclick="window.location.href = 'FAQs.php'"><br>
                        <input type="button" name="baboutus" class="btn btn-link button" value="About Us" style="font-size: 15pt" onclick="window.location.href = 'AboutUs.php'"><br>
                        <input type="button" name="bDelivery" class="btn btn-link button" value="Delivery & Return" style="font-size: 15pt"  onclick="window.location.href = 'deliveryAndReturn.php'"><br>
                        <input type="button" name="bprivacypolicy" class="btn btn-link button" value="Privacy Policy" style="font-size: 15pt"  onclick="window.location.href = 'privacyPolicy.php'">
                    </div>
                    </div>
                
            </div>
            

            <div class="col-lg-12">
                <div class="col-lg-6 text-right ">
                    <p>Email : <span class="text-orange"> customer.care@sam.com </span> </p>
                </div>
                <div class="col-lg-6 ">
                    <p>Phone : <span class="text-orange">+92 399 999 9999</span></p>
                </div>
            </div>
                    
                <div style="text-align:center;" class="col-lg-12">
                    <hr style="width: 100%">
                            
                        <div style="padding-top: 5px; margin-bottom: -20px;">
                            <img src="imgs/footerimgs/face.png" width="50" style="padding-right: 10px;">
                            <img src="imgs/footerimgs/pin.png" width="50" style="padding-right: 10px;">
                            <img src="imgs/footerimgs/twitter.png" width="50" style="padding-right: 10px;">
                        </div>
                    
                    <hr style="width: 100%">
                </div>
            <div class="col-lg-12">
                <center>
                    <p>&copy;AMS Furniture's</p>
                </center>
            </div>
    </div>
    
</footer>
</html>