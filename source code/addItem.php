<!DOCTYPE html>
<html>
<head>
  <title></title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="proto2Style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" src="proto2JS.js"></script>
    <script type="text/javascript" language="javascript">
      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(460)
                    .height(235);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }


    </script>

</head>
<body id="bg-body">


<!--NAVIGATION PANEL -->
<div class="container">

  <nav class="navbar-fixed-top" >
    <div class="container-fluid" style="padding-top: 30px;" >
      <div class="row" >
        <!-- LOGO -->
        <div class="col-sm-2" style="padding-left: 50px">
          <a href="home.php"><img src="imgs/logo/sam.png" width="100px" ></a>
        </div>
        <div class="col-sm-3" > <!-- SEARCH BAR -->
          <div class="input-group" style="padding-left: 50px; ">
            <input type="text" name="searchBar" placeholder="Search.." class="form-control">
            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
          </div>
        </div>
        <div class="col-sm-4"></div>
          <!-- loginSignup -->
        <div class="col-sm-3" style="font-size: 12pt; font-family: serif;" >
            <div style="margin-left: -180px;display: inline-block;">
              <span class="glyphicon glyphicon-earphone text-muted" ></span>
              <label class="navlbl" style="" >0399-9999999</label>
            </div>
              
            <div style="display: inline-block;">
              <a href="#" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
              <label class="navlbl" id="login"><a href="adminpanel.php"> ACCOUNT </label></a>
            </div>
            <div style="display: inline-block;">
              <a href="signout.php" ><span class="glyphicon glyphicon-user text-muted" ></span></a>
              <label class="navlbl" id="login"><a href="signout.php"> Sign Out </label></a>
            </div>
              
            <div style="display: inline-block;">
              <a href="cart.php" ><span class="glyphicon glyphicon-shopping-cart text-orange" ></span></a>
              <label class="navlbl"><a class="text-orange" href="cart.php"> My Cart </label></a>
            </div>
            
            <!--info-->

            <div class="col-sm-*" >
              <label class="text-orange" style="margin-bottom: 0px;">CASH ON DELIVERY | EASY PAY</label></br>
              <label style= "font-size: 12pt">Product Inquries/Checkout issues? Call Us</label>
            </div>
        </div>
          
      </div>
        
      <!-- PRIMARY MENU -->
      <div class="col-sm-12 prinav text-center" style="padding-bottom:10px  ;margin-right: 0px;margin-top: 20px; background-color: transparent;">
        <input class="btn btn-link navbtn" type="button" name="#BedRoom" value="Bed Room" onclick="window.location.href = 'bedroom.php'">
        <input class="btn btn-link navbtn" type="button" name="#Dining" value="Dining Room" onclick="window.location.href = 'dining.php'">
        <input class="btn btn-link navbtn" type="button" name="#Kitchen" value="Kitchen" onclick="window.location.href = 'kitchen.php'">
        <input class="btn btn-link navbtn" type="button" name="#Living" value="Living Room" onclick="window.location.href = 'living.php'">
        <input class="btn btn-link navbtn" type="button" name="#Office" value="Office" onclick="window.location.href = 'office.php'">
        <input class="btn btn-link navbtn" type="button" name="#addproduct" value="ADD PRODUCT" onclick="window.location.href = 'addItem.php'">
      </div>
      
    </div>  

  </nav>
</div>  

<!-- NAVIGATION END -->




<form method="post" enctype="multipart/form-data" action="add.php" class="form-horizontal" style="margin-top: 200px;">
<fieldset>

<!-- Form Name -->
<legend style="color:darkorange;" class="text-center">ADD PRODUCTS</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="product_categorie">PRODUCT CATEGORY</label>
  <div class="col-md-4">
    <select id="product_categorie" name="product_categorie" class="form-control">
      <option selected="selected" disabled hidden>Select Category</option>
      <option value="BED_ROOM">Bed Room</option>
      <option value="DINING_ROOM">Dining Room</option>
      <option value="KITCHEN">Kitchen</option>
      <option value="LIVING_ROOM">Living Room</option>
      <option value="OFFICE_ROOM">Office Room</option>
      <option value="SPECIAL_OFFER" disabled>Special Offer(currently disabled)</option>
    </select>
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="product_id">PRODUCT ID</label>  
  <div class="col-md-4">
    <input id="product_id" name="product_id" placeholder="PRODUCT ID" class="form-control input-md" required="" type="text" pattern="[A-Z]{3}-[0-9]{3}" >
  </div>
</div>
    

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="product_name">PRODUCT NAME</label>  
  <div class="col-md-4">
  <input id="product_name" name="product_name" placeholder="PRODUCT NAME" class="form-control input-md" required="" type="text" >
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="prod_price">PRODUCT PRICE</label>  
  <div class="col-md-4">
    <input id="prod_price" name="prod_price" placeholder="PRICE" class="form-control input-md" required="" type="number">
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="product_name_fr">PRODUCT SHORT DESCRIPTION</label>  
  <div class="col-md-4">
  <input id="product_name_fr" name="product_name_fr" placeholder="PRODUCT SHORT DESCRIPTION" class="form-control input-md" required="" type="text">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="available_quantity">AVAILABLE QUANTITY</label>  
  <div class="col-md-4">
  <input id="available_quantity" name="available_quantity" placeholder="AVAILABLE QUANTITY" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="product_weight">PRODUCT WEIGHT</label>  
  <div class="col-md-4">
  <input id="product_weight" name="product_weight" placeholder="PRODUCT WEIGHT" class="form-control input-md" required="" type="number">
    
  </div>
</div>    

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="author">COMPANY</label>  
  <div class="col-md-4">
  <input id="author" name="author" placeholder="COMPANY" class="form-control input-md" required="" type="text">
    
  </div>
</div>

 <!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="filebutton">Product Image</label>
  <div class="col-md-4">
    <!-- <input id="filebutton" name="filebutton" class="input-file" type="file"> -->
    <input type='file' onchange="readURL(this);" name="prod_image" id="prod_image" /><br>
    <img id="blah" src="#" alt="your image" />
   <blockquote>NOTE: Your Image Should be .jpg or .jpeg. And image Size of 460x235.</blockquote>
  </div>
</div>
<!-- Button -->
<div class="form-group">
  <!-- <label class="col-md-4 control-label" for="singlebutton">Publish</label> -->
    <button id="singlebutton" name="singlebutton" class="btn btn-primary"> Publish</button>
  <div class="col-md-4">
  </div>
</div>

</fieldset>
</form>

<script type="text/javascript" >
  
  $(document).ready(function() {
    $("#singlebutton").click(function () {
        var imageName = $("#prod_image").val();
        if (imageName == "") {
          alert("Please Select an Image");
          return false;
        }
        else{
          var extension = $("#prod_image").val().split('.').pop().toLowerCase();
          if (jQuery.inArray(extension, ['jpeg','jpg']) == -1 ) {
            alert("Invalid Image File. Please Make Sure Image should be .jpeg or .jpg format!");
            $("#prod_image").val("");
            return false; 
          }
        }
      // body...
    })
  })

</script>


</body>
</html>